

class initialize
{
  # Hiera lookup
  $init_packages = hiera('init::packages')

  package { $init_packages:
   ensure => installed
  }

  user { 'userKokoroe':
    name   => 'kokoroe',
    home   => '/home/kokoroe',
    managehome   => 'true',
    ensure => 'present',
    shell  => '/bin/bash'
  }
  file { "/home/kokoroe/.ssh":
    ensure  => directory,
    mode    => 0775,
    owner   => 'kokoroe',
    group   => 'kokoroe',
    require => User['userKokoroe'],
  }
  ssh_authorized_key { 'axelKey':
    name => 'axel@etcheverry.biz',
    user => 'kokoroe',
    type => 'ssh-rsa',
    key  => "AAAAB3NzaC1yc2EAAAADAQABAAACAQDUlTbUunzDSGnPJIP3QA1anjqzoZx5LnjXrSuquBInGvo2mmTHp30vThclG0NONYHt600bkcyk0H33WdnOde2Kxgjf5hazQAifgcteuIxB8039pviy+XO2f1EzJj4AZeyvy+vJ0rVR3LO/frmCeb84AX0lsZjLpLhfX5rCu3JcwdAgU8PVSEMSblM4gTz1A98IZkYYVUGJV0y6msNi/9OHs8IlzNYnEBFGShoAxseZbQCcajQupccukqCHd/znCMT2BoNdNqelf/RwqOBPyVNjFZkGC6T6LczOUazM9cwPmLJNmxMT8YlfihdjQFBEMd289TSAYjZCQTF7eQTeuxeYkniT8O3bcirq9nQEvbjhmNZqy0BOYXvf4FXGADOCu6G+sTc6rL1S0RCPkoakUeRFuo2sahgBDJSrbGwkm5OC7kWDzckg7pt+ajuKSKsdOpfP13a5paIb8y0AcqAR4adIrzOzdSMY1Mt8IfYcx3xauFJaDYaWytZAQdjtH/TdRk64ieGwKxIWpIZsxg3GPQj2Zss1ExEDuwkyCshrgkc1rZww2elO3oEpls2dCsFUD+g8IlV1uR5pP0NiYlFfRNrhqXcQ5XJhrFmy/kLwGytpBugfSk8pARiQhLcjrCS8JpAvs+4YG0LE6u/O/wNLtbW4ZFKBR6w6IW4jI5pJBj3oZw==",
    require => File['/home/kokoroe/.ssh'],
  }

  ssh_authorized_key { 'ruiKey':
    name => 'Rui@SURFACERUI',
    user => 'kokoroe',
    type => 'ssh-rsa',
    key  => "AAAAB3NzaC1yc2EAAAADAQABAAABAQD175XQ9WfhOPqKH35YVgeMFVK2ZZ+KNQLvqwGF3QSeVHCLlWasLnoSiVbnlAcjpOcAMQbzq3x/DXWxlcUgQe1B7X/a2yUNh0KHF7yipWnIzsDIBC6XeAPiX/zTWQDVCpT4uNnF1WsNHiFOsmFbLUWv/tCUnTpdsaolfDBTWRpMsr3rnaP7K1TFSTZbzBfoa9KDP1IaBuCUsjlBOBOz0Lsv43fzBeXT04DYswAdf7xUr6pM4AEmGeB7UbOOqxdZHqWnn3gQcPSkE7hqim54OgP4Wj0bmRZcshsTACP3aCl0gsGf8fWHXBIeBnl3VWDF9b38ZFW0YoJ5uMu9Tz3i0gFT",
    require => File['/home/kokoroe/.ssh'],
  }
  ssh_authorized_key { 'kieranKey':
    name => 'kin@kin-splio',
    user => 'kokoroe',
    type => 'ssh-rsa',
    key  => "AAAAB3NzaC1yc2EAAAADAQABAAABAQDV7Ha0Fi7Hp7NlkTa7WvtSW6TGHqNUcPFWXnG74tOzbCpCfVFURHTNwXq3Ub4IBnULPBewqRt4a3Qm/aF0pm+GLYc6lywDbTLeBFsvvQ6FQD+8da8W4PIFpIY3GNwB59KwkyFN5GTtddH/C5XaIZdzP7v/zyILOEv7ZQot/1141v1diUBekDyEe28za+0U498Z5qqTWLNIcWED4lKn06Xjvr3UrpiJu5t7q7k8PtaCwn0SHvY8wfquewcYDWPg3lA1kTSQNhLwkT5oPCw1DjBU5kj/SXCoK2bS75s+ccslDUU5FCNgbnUFroRTORm75oGj0qpqiXvcQwvZ0wAsaz9P",
    require => File['/home/kokoroe/.ssh'],
  }



}


class repo
{
  # Hiera lookup
  $repo_ondrej_ppa = hiera('repo::ondrej::ppa')
  $repo_ondrej_key = hiera('repo::ondrej::key')

  include apt

  apt::key { "ondrej": key => $repo_ondrej_key }
  apt::ppa { $repo_ondrej_ppa: 
    require => Apt::Key['ondrej'],
  }

}


class php
{

  # Hiera lookup
  $fpm_listen = hiera('fpm::listen')
  $fpm_user = hiera('fpm::user')
  $fpm_group = hiera('fpm::group')
  $fpm_allowed_clients = hiera('fpm::allowed_clients')
  $fpm_listen_owner = hiera('fpm::listen_owner')
  $fpm_listen_group = hiera('fpm::listen_group')
  $fpm_pm_max_children = hiera('fpm::pm_max_children')
  $fpm_pm_start_servers = hiera('fpm::pm_start_servers')
  $fpm_pm_min_spare_servers = hiera('fpm::pm_min_spare_servers')
  $fpm_pm_max_spare_servers = hiera('fpm::pm_max_spare_servers')
  $fpm_pm_max_requests = hiera('fpm::pm_max_requests')
  $fpm_php_admin_value = hiera('fpm::php_admin_value')

  include phpfpm

  phpfpm::pool { 'kokoroe':
      listen                 => $fpm_listen,
      user                   => $fpm_user,
      group                  => $fpm_group,
      listen_allowed_clients => $fpm_allowed_clients,
      listen_owner           => $fpm_listen_owner,
      listen_group           => $fpm_listen_group,
      pm_max_children        => $fpm_max_children,
      pm_start_servers       => $fpm_pm_start_servers,
      pm_min_spare_servers   => $fpm_pm_min_spare_servers,
      pm_max_spare_servers   => $fpm_pm_max_spare_servers,
      pm_max_requests        => $fpm_pm_max_requests,
      php_admin_value        => $fpm_php_admin_value,
  }

  # Hiera lookup
  $php_packages = hiera('php::package')
  $twig_version = hiera('php::twig_version')

  package { $php_packages:
    ensure  => installed,
  }

  exec { 'getTwig' :
    command => "wget https://github.com/twigphp/Twig/archive/v${twig_version}.tar.gz; tar -xvzf v${twig_version}.tar.gz",
    path    => '/bin:/sbin:/usr/bin:/usr/sbin',
    cwd     => '/usr/src',
    user    => 'root',
    group   => 'root',
    creates => '/usr/src/v${twig_version}.tar.gz',
    require => Package['php5-dev']
  }
  exec { 'CompileTwig' :
    command => '/usr/bin/phpize5 && ./configure && make && make install',
    path    => '/bin:/sbin:/usr/bin:/usr/sbin',
    cwd     => "/usr/src/Twig-${twig_version}/ext/twig",
    user    => 'root',
    creates => '/usr/lib/php5/20131226/twig.so',
    group   => 'root',
    require => Exec['getTwig']
  }
  exec { 'ActivateTwig' :
    command => "echo 'extension=twig.so' > /etc/php5/mods-available/twig.ini",
    path    => '/bin:/sbin:/usr/bin:/usr/sbin',
    cwd     => "/usr/src/Twig-${twig_version}/ext/twig",
    user    => 'root',
    unless  => "grep twig.so /etc/php5/mods-available/twig.ini",
    group   => 'root',
    require => Exec['CompileTwig'],
  }
  file { 'TwigCliSymlink':
    ensure => symlink,
    path   => '/etc/php5/cli/conf.d/25-twig.ini',
    target => "/etc/php5/mods-available/twig.ini",
  }
  file { 'TwigFpmSymlink':
    ensure => symlink,
    path   => '/etc/php5/fpm/conf.d/25-twig.ini',
    target => "/etc/php5/mods-available/twig.ini",
  }

}

class http
{

  class { 'nginx': }

  file { '/etc/nginx/sites-available/kokoroe':
    ensure => 'present',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    source => '/tmp/files/nginx/sites-available/kokoroe',
  }
  
  file { 'siteSymlink':
    ensure  => symlink,
    path    => '/etc/nginx/sites-enabled/kokoroe',
    target  => "/etc/nginx/sites-available/kokoroe",
    require => File['/etc/nginx/sites-available/kokoroe'],
  }

  file { '/etc/nginx/ssl':
    ensure  => 'directory',
    recurse => true,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    source  => '/tmp/files/nginx/ssl',
  }
}



class mail_relay
{

  include postfix

}

class rdis_master
{

   # Hiera lookup
   $redis_sentinel_master_name = hiera('redis::sentinel::master_name')
   $redis_sentinel_master_ip = hiera('redis::sentinel::master_ip')
   $redis_sentinel_parallel_sync = hiera('redis::sentinel::parallel_sync')
   $redis_sentinel_down_after = hiera('redis::sentinel::down_after')

  class { 'redis':
    bind    => $::ipaddress_eth0,
  }

  class { 'redis::sentinel':
    master_name      => $redis_sentinel_master_name,
    redis_host       => $redis_sentinel_master_ip,
    parallel_sync    => $redis_sentinel_parallel_sync,
    down_after       => $redis_sentinel_down_after,
  }

}

class rdis_slave
{

   # Hiera lookup
   $redis_sentinel_master_name = hiera('redis::sentinel::master_name')
   $redis_sentinel_master_ip = hiera('redis::sentinel::master_ip')
   $redis_sentinel_parallel_sync = hiera('redis::sentinel::parallel_sync')
   $redis_sentinel_down_after = hiera('redis::sentinel::down_after')

  class { 'redis':
    bind    => $::ipaddress_eth0,
    slaveof => "${redis_sentinel_master_name} 6379";
  }

  class { 'redis::sentinel':
    master_name      => $redis_sentinel_master_name,
    redis_host       => $redis_sentinel_master_ip,
    parallel_sync    => $redis_sentinel_parallel_sync,
    down_after       => $redis_sentinel_down_after,
  }

}

class nodejiess
{

  # Hiera lookup
  $nodeVersion = hiera('node::version')
  $nodePackages = hiera('node::packages')

  class { 'nodejs':
    version => "$nodeVersion",
  }
  file { 'nodeBinSymlink':
    ensure => symlink,
    path   => '/usr/local/bin/node',
    target => "/usr/local/node/node-${nodeVersion}/bin/node",
  }

  package { $nodePackages:
    ensure   => present,
    provider => 'npm',
    require  => Class['nodejs']
  }

}


stage { [ 'zero', 'init', 'pre', 'post' ] : }

Stage['zero'] -> Stage['init'] -> Stage['pre'] -> Stage['main'] -> Stage['post']

node default
{
  class { 'initialize' : stage => init }
  class { 'repo' : stage => init }
  class { 'http' : stage => pre }
  class { 'php' : stage => main }
  #class { 'mail_relay' : stage => main }
  class { 'rdis_master' : stage => main }
  #class { 'rdis_slave' : stage => main }
  class { 'nodejiess' : stage => main }

}
