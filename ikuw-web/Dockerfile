FROM ubuntu:14.04.1

MAINTAINER Kieran Joyeux <kieran.joyeux@gmail.com>

ENV REFRESHED_AT 11-02-2015
ENV PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
ENV TERM xterm

# Install Puppet & Facter
WORKDIR /usr/src
RUN apt-get update && apt-get install wget ruby locales aptitude apt-utils procps net-tools git -qqy
RUN wget --no-check-certificate https://apt.puppetlabs.com/puppetlabs-release-precise.deb
RUN dpkg -i puppetlabs-release-precise.deb
RUN apt-get update && apt-get install puppet facter -qqy

# Get puppet modules
RUN mkdir /tmp/modules/
WORKDIR /tmp/modules
RUN git clone https://github.com/puppetlabs/puppetlabs-apt apt/
RUN git clone https://github.com/puppetlabs/puppetlabs-stdlib stdlib/
RUN git clone https://github.com/puppetlabs/puppetlabs-concat concat/
RUN git clone https://github.com/jfryman/puppet-nginx nginx/
RUN git clone https://github.com/Slashbunny/puppet-phpfpm phpfpm/
RUN git clone https://github.com/example42/puppet-postfix postfix/
RUN git clone https://github.com/example42/puppi puppi/
RUN git clone https://github.com/arioch/puppet-redis redis/

RUN git clone https://github.com/willdurand/puppet-nodejs nodejs/
RUN git clone https://github.com/maestrodev/puppet-wget wget/


# Mount conf and Apply puppet masterless script
COPY files/ /tmp/files/
COPY manifests/ /tmp/manifests/
COPY hieradata/ /tmp/hieradata
COPY hiera.yaml /tmp/hiera.yaml
RUN puppet apply --modulepath /tmp/modules --hiera_config="/tmp/hiera.yaml" /tmp/manifests/ikuw-web.pp --debug

EXPOSE 80 443 

CMD ["redis-server", "/etc/redis/redis.conf"]

