#!/bin/bash

# Install Puppet
cd /usr/src
apt-get install git wget ruby locales aptitude apt-utils procps net-tools git -qqy
wget --no-check-certificate https://apt.puppetlabs.com/puppetlabs-release-precise.deb
dpkg -i puppetlabs-release-precise.deb
apt-get update && apt-get install puppet facter -qqy

# Mount conf and Apply puppet masterless script
mkdir /tmp/{files,manifests,hieradata}
cp -r files/* /tmp/files/
cp -r manifests/* /tmp/manifests/
cp -r hieradata/* /tmp/hieradata
cp -r hiera.yaml /tmp/hiera.yaml

# Get puppet modules
mkdir /tmp/modules/
cd /tmp/modules
git clone https://github.com/puppetlabs/puppetlabs-apt apt/
git clone https://github.com/puppetlabs/puppetlabs-stdlib stdlib/
git clone https://github.com/arioch/puppet-redis redis/

puppet apply --modulepath /tmp/modules --hiera_config="/tmp/hiera.yaml" /tmp/manifests/ikuw-redis.pp --debug

