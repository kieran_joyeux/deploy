

class initialize
{
  # Hiera lookup
  $init_packages = hiera('init::packages')

  package { $init_packages:
   ensure => installed
  }


  user { 'userKokoroe':
    name   => 'kokoroe',
    home   => '/home/kokoroe',
    managehome   => 'true',
    ensure => 'present',
    shell  => '/bin/bash'
  }
  file { "/home/kokoroe/.ssh":
    ensure  => directory,
    mode    => 0775,
    owner   => 'kokoroe',
    group   => 'kokoroe',
    require => User['userKokoroe'],
  }
  ssh_authorized_key { 'axelKey':
    name => 'axel@etcheverry.biz',
    user => 'kokoroe',
    type => 'ssh-rsa',
    key  => "AAAAB3NzaC1yc2EAAAADAQABAAACAQDUlTbUunzDSGnPJIP3QA1anjqzoZx5LnjXrSuquBInGvo2mmTHp30vThclG0NONYHt600bkcyk0H33WdnOde2Kxgjf5hazQAifgcteuIxB8039pviy+XO2f1EzJj4AZeyvy+vJ0rVR3LO/frmCeb84AX0lsZjLpLhfX5rCu3JcwdAgU8PVSEMSblM4gTz1A98IZkYYVUGJV0y6msNi/9OHs8IlzNYnEBFGShoAxseZbQCcajQupccukqCHd/znCMT2BoNdNqelf/RwqOBPyVNjFZkGC6T6LczOUazM9cwPmLJNmxMT8YlfihdjQFBEMd289TSAYjZCQTF7eQTeuxeYkniT8O3bcirq9nQEvbjhmNZqy0BOYXvf4FXGADOCu6G+sTc6rL1S0RCPkoakUeRFuo2sahgBDJSrbGwkm5OC7kWDzckg7pt+ajuKSKsdOpfP13a5paIb8y0AcqAR4adIrzOzdSMY1Mt8IfYcx3xauFJaDYaWytZAQdjtH/TdRk64ieGwKxIWpIZsxg3GPQj2Zss1ExEDuwkyCshrgkc1rZww2elO3oEpls2dCsFUD+g8IlV1uR5pP0NiYlFfRNrhqXcQ5XJhrFmy/kLwGytpBugfSk8pARiQhLcjrCS8JpAvs+4YG0LE6u/O/wNLtbW4ZFKBR6w6IW4jI5pJBj3oZw==",
    require => File['/home/kokoroe/.ssh'],
  }

  ssh_authorized_key { 'ruiKey':
    name => 'Rui@SURFACERUI',
    user => 'kokoroe',
    type => 'ssh-rsa',
    key  => "AAAAB3NzaC1yc2EAAAADAQABAAABAQD175XQ9WfhOPqKH35YVgeMFVK2ZZ+KNQLvqwGF3QSeVHCLlWasLnoSiVbnlAcjpOcAMQbzq3x/DXWxlcUgQe1B7X/a2yUNh0KHF7yipWnIzsDIBC6XeAPiX/zTWQDVCpT4uNnF1WsNHiFOsmFbLUWv/tCUnTpdsaolfDBTWRpMsr3rnaP7K1TFSTZbzBfoa9KDP1IaBuCUsjlBOBOz0Lsv43fzBeXT04DYswAdf7xUr6pM4AEmGeB7UbOOqxdZHqWnn3gQcPSkE7hqim54OgP4Wj0bmRZcshsTACP3aCl0gsGf8fWHXBIeBnl3VWDF9b38ZFW0YoJ5uMu9Tz3i0gFT",
    require => File['/home/kokoroe/.ssh'],
  }
  ssh_authorized_key { 'kieranKey':
    name => 'kin@kin-splio',
    user => 'kokoroe',
    type => 'ssh-rsa',
    key  => "AAAAB3NzaC1yc2EAAAADAQABAAABAQDV7Ha0Fi7Hp7NlkTa7WvtSW6TGHqNUcPFWXnG74tOzbCpCfVFURHTNwXq3Ub4IBnULPBewqRt4a3Qm/aF0pm+GLYc6lywDbTLeBFsvvQ6FQD+8da8W4PIFpIY3GNwB59KwkyFN5GTtddH/C5XaIZdzP7v/zyILOEv7ZQot/1141v1diUBekDyEe28za+0U498Z5qqTWLNIcWED4lKn06Xjvr3UrpiJu5t7q7k8PtaCwn0SHvY8wfquewcYDWPg3lA1kTSQNhLwkT5oPCw1DjBU5kj/SXCoK2bS75s+ccslDUU5FCNgbnUFroRTORm75oGj0qpqiXvcQwvZ0wAsaz9P",
    require => File['/home/kokoroe/.ssh'],
  }
}

class searchd 
{

  class { 'sphinx':
    config_file => '/tmp/files/sphinx/sphinx.conf',
  }

}


class sql
{

  # Hiera lookup
  $kokoroe_galera_server_list          = hiera('kokoroe::galera::server::list')
  $kokoroe_galera_server_master_name   = hiera('kokoroe::galera::server::master_name')
  $kokoroe_galera_server_root_password = hiera('kokoroe::galera::server::root_password')
  $kokoroe_galera_server_options       = hiera('kokoroe::galera::server::options')

  class { 'galera':
      galera_servers 	 => $kokoroe_galera_server_list,
      galera_master  	 => $kokoroe_galera_server_master_name,
      configure_firewall => false,
      configure_repo 	 => true,
      root_password 	 => $kokoroe_galera_server_root_password,
      override_options  => $kokoroe_galera_server_options,
  }

}


class redis-sentinel
{

  class { 'redis::sentinel':
    master_name      => $redis_sentinel_master_name,
    redis_host       => $redis_sentinel_master_ip,
    parallel_sync    => $redis_sentinel_parallel_sync,
    down_after       => $redis_sentinel_down_after,
  }

}

stage { [ 'zero', 'init', 'pre', 'post' ] : }

Stage['zero'] -> Stage['init'] -> Stage['pre'] -> Stage['main'] -> Stage['post']

node default
{
  class { 'initialize' : stage => init }
  class { 'searchd' : stage => pre }
  class { 'sql' : stage => main }
  class { 'redis-sentinel' : stage => main }

}
