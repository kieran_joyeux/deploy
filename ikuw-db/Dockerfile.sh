#!/bin/bash


# Install Puppet
cd /usr/src
apt-get install git wget ruby locales aptitude apt-utils procps net-tools git -qqy
wget --no-check-certificate https://apt.puppetlabs.com/puppetlabs-release-precise.deb
dpkg -i puppetlabs-release-precise.deb
apt-get update && apt-get install puppet facter -qqy

# Mount conf and Apply puppet masterless script
mkdir /tmp/{files,manifests,hieradata}
cp -r files/* /tmp/files/
cp -r manifests/* /tmp/manifests/
cp -r hieradata/* /tmp/hieradata
cp -r hiera.yaml /tmp/hiera.yaml

# Get puppet modules
mkdir /tmp/modules/
cd /tmp/modules
git clone https://github.com/puppetlabs/puppetlabs-apt apt/
git clone https://github.com/puppetlabs/puppetlabs-stdlib stdlib/
git clone https://github.com/puppetlabs/puppetlabs-mysql mysql/
git clone https://github.com/puppetlabs/puppetlabs-xinetd xinetd/
git clone https://github.com/michaeltchapman/puppet-galera galera/
git clone https://github.com/scalia/puppet-sphinx sphinx/
git clone https://github.com/arioch/puppet-redis redis/


# Replacing a classfile as this one is buggy...
cat > /tmp/modules/galera/manifests/debian.pp << "EOF"

# == Class: galera::debian
#
# Fixes Debian specific compatibility issues
#
class galera::debian {

  if ($::osfamily != 'Debian') {
    warn('the galera::debian class has been included on a non-debian host')
  }

  # Debian policy will autostart the non galera mysql after
  # package install, so kill it if the package is
  # installed during this puppet run
  exec { 'clean_up_ubuntu':
      command       => 'service mysql stop',
      path          => '/usr/bin:/bin:/usr/sbin:/sbin',
      refreshonly   => true,
      subscribe     => Package['mysql-server'],
      before        =>  Class['mysql::server::config'],
      require       => Class['mysql::server::install'],
  }

  if ($::fqdn == $galera::galera_master) {
    # Debian sysmaint pw will be set on the master,
    # and needs to be consistent across the cluster.
    mysql_user { 'debian-sys-maint@localhost':
      ensure        => 'present',
      password_hash => mysql_password($galera::deb_sysmaint_password),
      provider      => 'mysql',
      require       => File['/root/.my.cnf']
    }

    file { '/etc/mysql/debian.cnf':
      ensure    => present,
      owner     => 'mysql',
      group     => 'mysql',
      content   => template('galera/debian.cnf.erb'),
      require   => Mysql_user['debian-sys-maint@localhost']
    }
  }
}
EOF

# hack since the class is buggy 
sed -e "/require => File/d" -i /tmp/modules/galera/manifests/init.pp

# Apply puppet manifest
puppet apply --modulepath /tmp/modules --hiera_config="/tmp/hiera.yaml" /tmp/manifests/ikuw-db.pp --debug

